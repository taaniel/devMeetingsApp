import {Component, EventEmitter, OnInit, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators,} from '@angular/forms';
import {Subject} from 'rxjs';
import {DatabaseService} from './database.service';
import {filter, map, tap} from "rxjs/operators";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public messages;

  @Input() public submitButtonText = 'Submit';
  public form: FormGroup;

  formSubmitSubject = new Subject();


  constructor(private dataBase: DatabaseService, private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      sender: ['', Validators.required],
      message: ['', Validators.required],
      linkImage: [''],
      link: ['']
    });


    this.formSubmitSubject
      .pipe(
        filter(() => this.form.valid),
        map(() => this.form.value)
      )
      .subscribe((form) => {
        this.dataBase.sendMessage(form);
      });
  }

  compareFn = (a, b) => {
    if (a.timestamp.toDate() < b.timestamp.toDate())
      return -1;
    if (a.timestamp.toDate() > b.timestamp.toDate())
      return 1;
    return 0;
  };

  transformMessages(messages) {
    return messages.map(message => {
      message.time = message.timestamp.toDate();
      return message;
    });
  }

  ngOnInit(): void {
    this.messages = this.dataBase.getMessages()
      .pipe(
        map(messages => messages.sort(this.compareFn)),
        map(messages => this.transformMessages(messages)),
        tap(messages => console.log(messages))
      );
  }
}
