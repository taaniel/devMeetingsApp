import {Injectable} from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {Message} from "../model/message";

const collectionPath = 'messages';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(private db: AngularFirestore) {
  }

  getMessages() {
    return this.db.collection(collectionPath).valueChanges();
  }

  sendMessage(form) {
    let message: Message =
      {
        sender: form.sender,
        body: form.message,
        timestamp: new Date(),
        linkImage: form.linkImage,
        link: form.link
      };

    console.log(message)

    return this.db.collection(collectionPath).add(message);
  }
}
