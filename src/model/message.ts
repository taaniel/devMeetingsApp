export class Message {
  sender: string;
  body: string;
  timestamp: Date;
  linkImage?: string;
  link?: string;
}
